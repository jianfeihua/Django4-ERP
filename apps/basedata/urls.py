from django.urls import re_path as url
import basedata.views

urlpatterns = [
    url(r"dataimport/(?P<object_id>\d+)/action", basedata.views.action_import),
]
