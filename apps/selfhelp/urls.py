from django.urls import re_path as url
import selfhelp.views

urlpatterns = [
    url(r"(?P<model>\w+)/(?P<object_id>\d+)/pay", selfhelp.views.pay_action),
]
