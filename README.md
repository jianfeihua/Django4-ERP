# Django4-ERP

Django4-ERP是一款基于Django4开发的ERP管理软件，包含常用的销售管理、采购管理、库存管理、组织管理等，支持按项目归集费用，支持工作流审批，支持采购单、报价单的批量导入。

Forked from `<a href="https://github.com/zhuinfo/Django-ERP">`zhuinfo Django-ERP`</a>` 感谢他的付出。

# 安装指南

> 我的开发、测试环境是Python3.10的，所以这个文档大多数情况我默认会使用这个条件，有个别测试不到位的可能还需要慢慢完善。

请先确保您已安装了Python 3.10,并已配置好了数据库，本文档会略过这部分内容（理论上Django是可以支持MYSql、PGSQL、SQLite、Oracle等主流数据库的，但是建议不要嘬，用自己熟悉的数据库，因为数据是无价的。）
验证方法请通过python --version查看版本，以及数据库 确认用户名和密码是否登录正常

## 👉 下载代码

```bash
$ git clone https://gitee.com/jianfeihua/Django4-ERP.git
$ cd Django4-ERP
```

<br />

## 👉 在虚拟环境"VENV"安装需求库

```bash
$ python -m venv env
$ .\env\Scripts\activate;
$ pip install -r ./install/requirements.txt
```

<br />

## 👉 配置settings

```bash
    INSTALLED_APPS = (
        ...
        'admin_adminlte.apps.AdminAdminlteConfig',
        'django.contrib.admin',
    )
    LOGIN_REDIRECT_URL = '/'
    # EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'    
```

<br />

## 👉 配置路由

```bash
    from django.urls import path, include

    urlpatterns = [
        ...
        path('', include('admin_adminlte.urls')),
    ]    
```

<br />

## 👉 收集静态资源

```bash
    python manage.py collectstatic   
```

<br />

## 👉 设置数据库

```bash
$ python manage.py makemigrations
$ python manage.py migrate
```

<br />

## 👉 创建超级用户

```bash
$ python manage.py createsuperuser
```

<br />

## 👉 启动应用程序

```bash
$ python manage.py runserver
```

此时应用程序运行在： `http://127.0.0.1:8000/`

<br />

## 👉 如何定制

* Step 1：在应用内创建目录templates
* Step 2：将项目配置为使用此新模板目录, 编辑settings.py的TEMPLATES 部分
* Step 3：从原始位置（在您的 ENV 中）复制并将其保存到YOUR_APP/templates/includes/footer.html
* 源路径：<YOUR_ENV>/LIB/admin_adminlte/templates/includes/footer.html
* 目标路径：YOUR_APP/templates/includes/footer.html
* 编辑 （目标路径）footer.html
* 此时，Django 会忽略库中附带的默认版本。footer.html

以类似的方式，所有其他文件和组件都可以轻松自定义。

<br />

# 排错

## MYSQL驱动错误

```
django.core.exceptions.ImproperlyConfigured: Error loading MySQLdb module: No module named MySQLdb
```

出现No module named MySQLdb是django找不到MySQL驱动导致的问题，所以需要先安装一个数据库驱动。
